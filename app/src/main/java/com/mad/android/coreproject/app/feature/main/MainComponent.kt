package com.mad.android.coreproject.app.feature.main

import dagger.Module
import dagger.Subcomponent

@Subcomponent(modules = [MainModule::class])
interface MainComponent {

    fun inject(mainViewModel : MainViewModel)
}

@Module
class MainModule