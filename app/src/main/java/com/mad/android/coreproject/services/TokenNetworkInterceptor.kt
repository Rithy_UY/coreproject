package com.mad.android.coreproject.services

import okhttp3.Interceptor
import okhttp3.Response


class TokenNetworkInterceptor (private val mAuthorizationToken: String) : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        val requestBuilder = chain.request().newBuilder()
        requestBuilder.header("Content-type", "application/json")
        requestBuilder.addHeader("AuthorizationToken", mAuthorizationToken).build()
        return chain.proceed(requestBuilder.build())
    }
}