package com.mad.android.coreproject.models

import io.reactivex.subjects.PublishSubject
import io.reactivex.subjects.Subject
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class UserManager @Inject constructor() {

    private var mUserProfile: UserProfile? = null
    val onUserDataChange: Subject<UserProfile> = PublishSubject.create()

    fun isLoggedIn(): Boolean {
        return mUserProfile != null
    }

    fun getUserProfile(): UserProfile? {
        return mUserProfile
    }

    fun setUserProfile(userProfile: UserProfile){
        mUserProfile = userProfile
        onUserDataChange.onNext(userProfile)
    }

    fun deleteUser() {
        mUserProfile = null
    }
}

