package com.mad.android.coreproject.services.models

import com.google.gson.annotations.SerializedName

class BasicInfo {

    @SerializedName("question")
    lateinit var question: String

    @SerializedName("published_at")
    lateinit var publishedAt: String

    @SerializedName("choices")
    lateinit var choices: ArrayList<Language>

    @SerializedName("architectures")
    lateinit var architectures: ArrayList<Architecture>

}

class Language {

    @SerializedName("choice")
    lateinit var languageName: String

    @SerializedName("logo")
    lateinit var logo: String
}

class Architecture {
    @SerializedName("name")
    lateinit var Name: String
}
