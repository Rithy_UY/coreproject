package com.mad.android.coreproject.services

import com.mad.android.coreproject.BuildConfig
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import com.google.gson.GsonBuilder
import com.google.gson.Gson

/**
 * @author: Rithy Uy
 * email: rithy@workwithmad.com
 */

object ServiceFactory {

    private const val DATE_FORMAT : String = "yyyy-MM-dd'T'HH:mm:ss"

    fun <T> create(targetApiService: Class<T>, authenticatorToken: String = ""): T {

        return Retrofit.Builder()
                .baseUrl(BuildConfig.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(createGsonDateConverter()))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(createOkHttpClient(authenticatorToken))
                .build()
                .create(targetApiService)
    }

    /** @param authenticatorToken is the token that you got after logged in **/
    private fun createOkHttpClient(authenticatorToken: String = ""): OkHttpClient {

        val okHttpClientBuilder = OkHttpClient().newBuilder()
        val loggingInterceptor = HttpLoggingInterceptor()
        loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY

        return okHttpClientBuilder
                .addInterceptor(loggingInterceptor)
                .addNetworkInterceptor(TokenNetworkInterceptor(authenticatorToken))
                .readTimeout(BuildConfig.DEFAULT_TIME_OUT, TimeUnit.SECONDS)
                .connectTimeout(BuildConfig.DEFAULT_TIME_OUT, TimeUnit.SECONDS)
                .build()
    }

    private fun createGsonDateConverter(): Gson {

        return GsonBuilder()
                .setDateFormat(DATE_FORMAT)
                .create()
    }
}