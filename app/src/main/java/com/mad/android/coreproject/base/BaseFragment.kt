package com.mad.android.coreproject.base

import android.databinding.DataBindingUtil
import android.databinding.ViewDataBinding
import android.os.Bundle
import android.support.annotation.IdRes
import android.support.annotation.LayoutRes
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

abstract class BaseFragment<B : ViewDataBinding, out VM : BaseViewModel<*>> : Fragment() {

    protected abstract val layoutResId: Int @LayoutRes get
    protected abstract val variable: Int @IdRes get
    protected abstract val viewModel: VM
    private lateinit var mBinding: B

    open fun showGlobalLoading() {}
    open fun hideGlobalLoading() {}

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        mBinding = DataBindingUtil.inflate(inflater, layoutResId,container,false)
        mBinding.setVariable(variable,variable)
        mBinding.executePendingBindings()
        return mBinding.root
    }
}