package com.mad.android.coreproject.app.feature.main

import android.content.Context
import android.databinding.DataBindingUtil
import android.databinding.ViewDataBinding
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.mad.android.coreproject.R
import com.mad.android.coreproject.services.models.Language
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.programing_item_view.view.*

class ProgrammingRecyclerViewAdapter(
        private val context: Context,
        private val mLanguages: ArrayList<Language>) : RecyclerView.Adapter<ProgrammingRecyclerViewAdapter.ProgrammingViewHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProgrammingViewHolder {
        val inflater : LayoutInflater = LayoutInflater.from(context)
        return ProgrammingViewHolder(DataBindingUtil.inflate(inflater, R.layout.programing_item_view,parent,false))
    }

    override fun getItemCount(): Int = mLanguages.size

    override fun onBindViewHolder(holder: ProgrammingViewHolder, position: Int) {
        holder.bindView(mLanguages[position])
    }

    inner class ProgrammingViewHolder(dataBinding: ViewDataBinding) : RecyclerView.ViewHolder(dataBinding.root){
        fun bindView(choice: Language){
            Picasso.get().load(choice.logo).into(itemView.img_language_logo)
        }
    }
}