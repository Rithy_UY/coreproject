package com.mad.android.coreproject.app

import android.app.Application
import com.mad.android.coreproject.app.di.AppComponent
import com.mad.android.coreproject.app.di.AppModule
import com.mad.android.coreproject.app.di.DaggerAppComponent

class App : Application() {

    val appComponent : AppComponent by lazy {
        DaggerAppComponent
                .builder()
                .appModule(AppModule(this))
                .build()
    }

    override fun onCreate() {
        super.onCreate()
        appComponent.inject(this)
    }
}