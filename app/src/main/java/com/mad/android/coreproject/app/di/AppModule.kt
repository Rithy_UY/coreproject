package com.mad.android.coreproject.app.di

import com.mad.android.coreproject.app.App
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule(val app: App) {

    @Provides
    @Singleton
    fun provideApp() = app
}