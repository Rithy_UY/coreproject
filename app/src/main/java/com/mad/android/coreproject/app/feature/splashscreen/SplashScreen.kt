package com.mad.android.coreproject.app.feature.splashscreen

import android.content.Intent
import android.graphics.Color
import com.mad.android.coreproject.app.App
import com.mad.android.coreproject.models.UserManager
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import com.mad.android.coreproject.BuildConfig
import com.mad.android.coreproject.R
import com.mad.android.coreproject.app.feature.main.MainActivity
import com.squareup.picasso.Picasso
import io.reactivex.Flowable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_splash.*
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class SplashScreen : AppCompatActivity() {

    @Inject
    lateinit var userManager: UserManager
    private lateinit var disposable: Disposable

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        (application as App).appComponent.plus(SplashScreenModule()).inject(this)
        Picasso.get().load(BuildConfig.MAD_LOGO).into(imageView)
        disposable = Flowable
                .just(this)
                .delay(2000, TimeUnit.MILLISECONDS)
                .observeOn(Schedulers.io())
                .subscribeOn(AndroidSchedulers.mainThread())
                .subscribe { navigateUI() }
    }

    private fun navigateUI() {
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
        finish()
    }

    override fun onDestroy() {
        super.onDestroy()
        disposable.dispose()
    }
}