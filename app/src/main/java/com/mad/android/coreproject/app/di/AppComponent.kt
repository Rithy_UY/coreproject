package com.mad.android.coreproject.app.di

import com.mad.android.coreproject.app.App
import com.mad.android.coreproject.app.feature.main.MainComponent
import com.mad.android.coreproject.app.feature.main.MainModule
import com.mad.android.coreproject.app.feature.splashscreen.SplashScreenComponent
import com.mad.android.coreproject.app.feature.splashscreen.SplashScreenModule
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [AppModule::class])
interface AppComponent {

    fun inject(app: App)

    fun plus(splashScreen: SplashScreenModule) : SplashScreenComponent

    fun plus(main: MainModule) : MainComponent
}