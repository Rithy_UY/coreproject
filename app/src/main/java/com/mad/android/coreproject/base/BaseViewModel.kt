package com.mad.android.coreproject.base

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.databinding.BaseObservable
import android.support.v4.app.Fragment
import android.widget.Toast
import java.io.IOException
import java.io.Serializable

/**
 *  @author Rithy UY
 *  email: rithy@workwithmad.com
 */

@Suppress("UNCHECKED_CAST")
abstract class BaseViewModel<V : BaseView>(private val context: Context, private val view: BaseView) : BaseObservable() {

    open fun onActivityCreated() {}

    open fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {}

    open fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {}

    /**
     *  @param targetActivityClass is for the activity that you want to go
     *  @param isFinish if you want to finished current request activity
     *  @param data if you want to pass data to target activity
     */
    protected fun startActivity(targetActivityClass: Class<out Activity>, isFinish: Boolean = false, data: Serializable? = null) {
        val intent = Intent(context, targetActivityClass)
        intent.putExtra("data", data)
        (context as Activity).startActivity(intent)
        if (isFinish) context.finish()
    }

    protected fun startActivityForResult(targetActivityClass: Class<out Activity>, requestCode: Int, data: Serializable? = null) {
        val intent = Intent(context, targetActivityClass)
        intent.putExtra("data", data)
        (context as Activity).startActivityForResult(intent, requestCode)
    }

    /** @param fragment if the activity is request from fragment */
    protected fun startActivityForResult(fragment: Fragment, targetActivityClass: Class<out Activity>, requestCode: Int, data: Serializable? = null) {
        val intent = Intent(context, targetActivityClass)
        intent.putExtra("data", data)
        fragment.startActivityForResult(intent, requestCode)
    }

    @Throws(IOException::class)
    protected fun <T : Serializable> getIntentDataAsSerializable(name: String = "data"): T {
        return (context as Activity).intent.getSerializableExtra(name) as T
    }

    protected fun toast(message: String) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
    }

    protected fun finish() {
        (context as? Activity)?.finish()
    }

    fun getView(): V {
        return view as V
    }

    open fun onDisposableRx() {}
}