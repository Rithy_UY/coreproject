package com.mad.android.coreproject.app.feature.main

import android.animation.ObjectAnimator
import android.support.v7.widget.LinearLayoutManager
import android.text.method.LinkMovementMethod
import android.view.View
import com.mad.android.coreproject.BR
import com.mad.android.coreproject.R
import com.mad.android.coreproject.app.di.AppComponent
import com.mad.android.coreproject.base.BaseActivity
import com.mad.android.coreproject.base.BaseView
import com.mad.android.coreproject.databinding.ActivityMainBinding
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : BaseActivity<ActivityMainBinding, MainViewModel>(), BaseView {

    override val layoutResId: Int = R.layout.activity_main
    override val variable: Int = BR.viewModel
    override val viewModel by lazy { MainViewModel(this, this) }
    private lateinit var mProgrammingRecyclerViewAdapter: ProgrammingRecyclerViewAdapter

    override fun setupComponent(appComponent: AppComponent) {
        appComponent.plus(MainModule()).inject(viewModel)
        setupProgrammingRecyclerView()
    }

    private fun setupProgrammingRecyclerView() {
        tvFooter.movementMethod = LinkMovementMethod.getInstance()
        mProgrammingRecyclerViewAdapter = ProgrammingRecyclerViewAdapter(this, viewModel.mainModel.choices)
        rvProgramming.layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        rvProgramming.adapter = mProgrammingRecyclerViewAdapter
    }

    fun onDataModelChange() {
        mProgrammingRecyclerViewAdapter.notifyDataSetChanged()
    }

    override fun showGlobalLoading() {
        global_loading_view.visibility = View.VISIBLE
        containerView.visibility = View.GONE
    }

    override fun hideGlobalLoading() {
        global_loading_view.visibility = View.GONE
        containerView.visibility = View.VISIBLE
        animationContainerView(0f, 1f)
    }

    private fun animationContainerView(fromValues: Float, toValues: Float, duration: Long = 1000) {
        ObjectAnimator.ofFloat(containerView, "alpha", fromValues, toValues).setDuration(duration).start()
    }
}