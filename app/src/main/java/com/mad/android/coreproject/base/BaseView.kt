package com.mad.android.coreproject.base

interface BaseView {

    fun showGlobalLoading()
    fun hideGlobalLoading()
}