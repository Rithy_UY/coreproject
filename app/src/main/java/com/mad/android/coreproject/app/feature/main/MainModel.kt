package com.mad.android.coreproject.app.feature.main

import android.databinding.ObservableField
import com.mad.android.coreproject.services.models.Architecture
import com.mad.android.coreproject.services.models.BasicInfo
import com.mad.android.coreproject.services.models.Language

/**
 * @author Rithy UY
 * email: rithy@workwithmad.com
 */

@Suppress("MemberVisibilityCanBePrivate")
class MainModel {

    private val architectures : ArrayList<Architecture> = arrayListOf()
    private lateinit var mPublishedAt: String
    private lateinit var mQuestion: String
    val choices : ArrayList<Language> = arrayListOf()
    val displayPublishedAt = ObservableField<String>()
    val displayQuestion = ObservableField<String>()

    var publishedAt: String
        set(value) {
            mPublishedAt = value
            displayPublishedAt.set(value)
        }
        get() = mPublishedAt

    var question: String
        set(value) {
            mQuestion = value
            displayQuestion.set(mQuestion)
        }
        get() = mQuestion

    fun mapFrom(basicInfo: BasicInfo)  : MainModel {
        this.publishedAt = basicInfo.publishedAt
        this.question = basicInfo.question
        this.choices.addAll(basicInfo.choices)
        this.architectures.addAll(basicInfo.architectures)
        return this
    }
}