package com.mad.android.coreproject.app.feature.main

import android.content.Context
import android.util.Log
import com.mad.android.coreproject.base.BaseView
import com.mad.android.coreproject.base.BaseViewModel
import com.mad.android.coreproject.services.ServiceFactory
import com.mad.android.coreproject.services.apiservices.MadApiService
import com.mad.android.coreproject.services.models.BasicInfo
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import retrofit2.HttpException

class MainViewModel(context: Context, view: BaseView) : BaseViewModel<MainActivity>(context, view) {

    private val mMadApiService by lazy { ServiceFactory.create(MadApiService::class.java) }
    val mainModel by lazy { MainModel() }

    override fun onActivityCreated() {
        mMadApiService
                .requestMadBasicInfo()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { getView().showGlobalLoading() }
                .subscribe(this::onRequestSuccess, this::onRequestError, this::onRequestCompleted)
    }

    private fun onRequestSuccess(basicInfo: BasicInfo) {
        mainModel.mapFrom(basicInfo)
        getView().onDataModelChange()
    }

    private fun onRequestError(error: Throwable) {
        if(error is HttpException) onHttpException(error)
        getView().hideGlobalLoading()
        toast(error.message.toString())
    }

    private fun onRequestCompleted() {
        getView().hideGlobalLoading()
    }

    private fun onHttpException(error: HttpException){
        Log.e("RESPONSE_CODE", error.code().toString())
    }
}