package com.mad.android.coreproject.app.feature.splashscreen

import dagger.Module
import dagger.Subcomponent

@Subcomponent(modules = [SplashScreenModule::class])
interface SplashScreenComponent {
    fun inject(splashScreen: SplashScreen)
}

@Module
open class SplashScreenModule