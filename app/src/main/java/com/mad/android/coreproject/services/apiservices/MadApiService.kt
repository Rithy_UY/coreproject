package com.mad.android.coreproject.services.apiservices

import com.mad.android.coreproject.services.models.BasicInfo
import io.reactivex.Observable
import retrofit2.http.GET

interface MadApiService {

    @GET("/basic_info")
    fun requestMadBasicInfo() : Observable<BasicInfo>
}