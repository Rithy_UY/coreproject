package com.mad.android.coreproject.base

import android.content.Intent
import android.databinding.DataBindingUtil
import android.databinding.ViewDataBinding
import com.mad.android.coreproject.app.App
import com.mad.android.coreproject.app.di.AppComponent
import android.os.Bundle
import android.support.annotation.IdRes
import android.support.annotation.LayoutRes
import android.support.v7.app.AppCompatActivity

/**
 * @author Rithy UY
 * email: rithy@workwithmad.com
 */

abstract class BaseActivity<B : ViewDataBinding, out VM : BaseViewModel<*>> : AppCompatActivity() {

    protected abstract val layoutResId: Int @LayoutRes get
    protected abstract val variable: Int @IdRes get
    protected abstract val viewModel: VM
    private lateinit var mBinding: B

    open fun showGlobalLoading() {}
    open fun hideGlobalLoading() {}

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = DataBindingUtil.setContentView(this, layoutResId)
        mBinding.setVariable(variable, viewModel)
        setupComponent((application as App).appComponent)
        viewModel.onActivityCreated()
    }

    abstract fun setupComponent(appComponent: AppComponent)

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        viewModel.onActivityResult(requestCode, resultCode, data)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        viewModel.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    override fun onDestroy() {
        super.onDestroy()
        viewModel.onDisposableRx()
    }
}