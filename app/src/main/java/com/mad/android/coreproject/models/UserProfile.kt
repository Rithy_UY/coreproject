package com.mad.android.coreproject.models

data class UserProfile(
        val id: Int,
        var token: String,
        var userName: String,
        val description: String,
        val profileUrl: String
)