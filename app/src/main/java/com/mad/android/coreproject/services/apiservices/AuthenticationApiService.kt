package com.mad.android.coreproject.services.apiservices

import io.reactivex.Observable
import retrofit2.http.POST


interface AuthenticationApiService {

    @POST("/login")
    fun login(): Observable<LoginResponse>

}

data class LoginResponse(
        val userName: String,
        val prfileURL: String,
        val bio: String,
        val description: String
)